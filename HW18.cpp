﻿#include <iostream>

const int size = 3;

struct MyStack
{
	int arrayForStack[size]; 
	int countInStack; 
};

void BuildStack(MyStack* p)
{
	p->countInStack = 0;
}

bool IsStackIsFull(MyStack* p)
{
	if (p->countInStack == 0)
	{
		return true;
	}
	else if (p->countInStack == size)
	{
		return false;
	}
}

void AddInStack(MyStack* p)
{
	int userInputInt;
	std::cout << "Enter any int for add in stack > ";
	std::cin >> userInputInt;
	p->arrayForStack[p->countInStack] = userInputInt;
	p->countInStack++;
}

void DeleteFromStack(MyStack* p)
{
	p->countInStack--;
}

int GetTopInStack(MyStack* p)
{
	return p->arrayForStack[p->countInStack - 1];
}

void main()
{
	MyStack myStack;
	BuildStack(&myStack);
	char userInputNumber;

	do
	{
		std::cout << "\n0. Exit program" << "\n";
		std::cout << "1. PUSH: Add int to stack" << "\n";
		std::cout << "2. POP: remove int from stack " << "\n";
		std::cout << "\nEnter command number  ";
		std::cin >> userInputNumber;

		switch (userInputNumber)
		{
		case '1':
			if (IsStackIsFull(&myStack) == false)
			{
				std::cout << "\nStack is full\n";
			}
			else
			{
				AddInStack(&myStack);
				std::cout << "\nNew int add to stack\n\n";
			} break;

		case '2':
			if (IsStackIsFull(&myStack) == true)
			{
				std::cout << "\nStack is empty\n";
			}
			else
			{
				DeleteFromStack(&myStack);
				std::cout << "\nint was deleted from stack\n";
			} break;

		case '0': break;

		default: std::cout << "\nincorrect input...\n";
			break;
		}

		system("pause");
		system("cls");
	} while (userInputNumber != '0');
}